﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btn_disconnect = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.btn_connection_check = new System.Windows.Forms.Button();
            this.lb_is_uni_hs = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.password = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Used = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Recharge_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lb_status = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lb_version = new System.Windows.Forms.Label();
            this.lklb_update = new System.Windows.Forms.LinkLabel();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_connect = new System.Windows.Forms.Button();
            this.lb_nexttry = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_disconnect
            // 
            this.btn_disconnect.BackColor = System.Drawing.Color.Coral;
            this.btn_disconnect.Enabled = false;
            this.btn_disconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_disconnect.ForeColor = System.Drawing.Color.Black;
            this.btn_disconnect.Location = new System.Drawing.Point(418, 12);
            this.btn_disconnect.Name = "btn_disconnect";
            this.btn_disconnect.Size = new System.Drawing.Size(75, 23);
            this.btn_disconnect.TabIndex = 0;
            this.btn_disconnect.Text = "Disconnect";
            this.btn_disconnect.UseVisualStyleBackColor = false;
            this.btn_disconnect.Click += new System.EventHandler(this.btn_disconnect_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // btn_connection_check
            // 
            this.btn_connection_check.Location = new System.Drawing.Point(26, 12);
            this.btn_connection_check.Name = "btn_connection_check";
            this.btn_connection_check.Size = new System.Drawing.Size(75, 23);
            this.btn_connection_check.TabIndex = 2;
            this.btn_connection_check.Text = "Check";
            this.btn_connection_check.UseVisualStyleBackColor = true;
            this.btn_connection_check.Click += new System.EventHandler(this.btn_connection_check_Click);
            // 
            // lb_is_uni_hs
            // 
            this.lb_is_uni_hs.AutoSize = true;
            this.lb_is_uni_hs.Location = new System.Drawing.Point(5, 38);
            this.lb_is_uni_hs.Name = "lb_is_uni_hs";
            this.lb_is_uni_hs.Size = new System.Drawing.Size(117, 13);
            this.lb_is_uni_hs.TabIndex = 3;
            this.lb_is_uni_hs.Text = "connected to unknown";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.username,
            this.password,
            this.Size,
            this.Used,
            this.Recharge_date});
            this.dataGridView1.Location = new System.Drawing.Point(8, 71);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(589, 283);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDoubleClick);
            this.dataGridView1.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dataGridView1_UserDeletedRow);
            // 
            // username
            // 
            this.username.HeaderText = "username";
            this.username.Name = "username";
            // 
            // password
            // 
            this.password.HeaderText = "password";
            this.password.Name = "password";
            // 
            // Size
            // 
            this.Size.HeaderText = "Size";
            this.Size.Name = "Size";
            // 
            // Used
            // 
            this.Used.HeaderText = "Used";
            this.Used.Name = "Used";
            // 
            // Recharge_date
            // 
            this.Recharge_date.HeaderText = "Recharge date";
            this.Recharge_date.Name = "Recharge_date";
            // 
            // lb_status
            // 
            this.lb_status.AutoSize = true;
            this.lb_status.Location = new System.Drawing.Point(453, 38);
            this.lb_status.Name = "lb_status";
            this.lb_status.Size = new System.Drawing.Size(82, 13);
            this.lb_status.TabIndex = 5;
            this.lb_status.Text = "status:unknown";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 368);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 39);
            this.label1.TabIndex = 6;
            this.label1.Text = "Kashanu.ac.ir auto HS loginner\r\nDeveloped by Mahdi Rafatjah\r\nRafatjah.Mahdi@gmail" +
    ".com";
            // 
            // lb_version
            // 
            this.lb_version.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_version.AutoSize = true;
            this.lb_version.Location = new System.Drawing.Point(524, 394);
            this.lb_version.Name = "lb_version";
            this.lb_version.Size = new System.Drawing.Size(0, 13);
            this.lb_version.TabIndex = 7;
            // 
            // lklb_update
            // 
            this.lklb_update.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lklb_update.AutoSize = true;
            this.lklb_update.Location = new System.Drawing.Point(515, 394);
            this.lklb_update.Name = "lklb_update";
            this.lklb_update.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lklb_update.Size = new System.Drawing.Size(77, 13);
            this.lklb_update.TabIndex = 8;
            this.lklb_update.TabStop = true;
            this.lklb_update.Text = "update version";
            this.lklb_update.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lklb_update_LinkClicked);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(212, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Get Hs\'s Data from IBSNG";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.process_ibsng_data_btn);
            // 
            // btn_connect
            // 
            this.btn_connect.BackColor = System.Drawing.Color.PaleGreen;
            this.btn_connect.Enabled = false;
            this.btn_connect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_connect.ForeColor = System.Drawing.Color.Black;
            this.btn_connect.Location = new System.Drawing.Point(499, 12);
            this.btn_connect.Name = "btn_connect";
            this.btn_connect.Size = new System.Drawing.Size(75, 23);
            this.btn_connect.TabIndex = 11;
            this.btn_connect.Text = "Connect";
            this.btn_connect.UseVisualStyleBackColor = false;
            this.btn_connect.Click += new System.EventHandler(this.btn_connect_Click);
            // 
            // lb_nexttry
            // 
            this.lb_nexttry.AutoSize = true;
            this.lb_nexttry.Location = new System.Drawing.Point(36, -2);
            this.lb_nexttry.Name = "lb_nexttry";
            this.lb_nexttry.Size = new System.Drawing.Size(50, 13);
            this.lb_nexttry.TabIndex = 12;
            this.lb_nexttry.Text = "next try:0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 416);
            this.Controls.Add(this.btn_connect);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lklb_update);
            this.Controls.Add(this.lb_version);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lb_status);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lb_is_uni_hs);
            this.Controls.Add(this.btn_connection_check);
            this.Controls.Add(this.btn_disconnect);
            this.Controls.Add(this.lb_nexttry);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Kashan university Hs manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_disconnect;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Button btn_connection_check;
        private System.Windows.Forms.Label lb_is_uni_hs;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lb_status;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lb_version;
        private System.Windows.Forms.LinkLabel lklb_update;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn username;
        private System.Windows.Forms.DataGridViewTextBoxColumn password;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size;
        private System.Windows.Forms.DataGridViewTextBoxColumn Used;
        private System.Windows.Forms.DataGridViewTextBoxColumn Recharge_date;
        private System.Windows.Forms.Button btn_connect;
        private System.Windows.Forms.Label lb_nexttry;
    }
}

