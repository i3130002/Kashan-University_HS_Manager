﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Net;
using System.Collections.Specialized;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        int version = 2;
        public Form1()
        {
            InitializeComponent();
        }

       

        private void button2_Click(object sender, EventArgs e)
        {

        }


        int next_refresh = 10;
        int refreshed_connected_count = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            next_refresh--;
            if(next_refresh == 0)
            {
                btn_connection_check_Click(sender, e);
                if (hs.is_loged_in)
                    refreshed_connected_count++;
                else
                    refreshed_connected_count--;

                if(Math.Abs(refreshed_connected_count) < 3)
                {
                    next_refresh = 10;
                    
                }
                else if (Math.Abs(refreshed_connected_count) < 20)
                {
                    next_refresh = 60;
                }
                else 
                {
                    next_refresh = 600;
                }

            }
            lb_nexttry.Text  ="Next refresh:"+next_refresh.ToString();
        }
        //private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenu contextMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem menuItem4;

        //  private System.ComponentModel.IContainer components;

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {

            var window = MessageBox.Show("You can close this app using try icon");
            e.Cancel = true;
            this.WindowState = FormWindowState.Minimized;
            return;
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            this.components = new System.ComponentModel.Container();
            this.contextMenu1 = new System.Windows.Forms.ContextMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();

            // Initialize contextMenu1
            this.contextMenu1.MenuItems.AddRange(
                        new System.Windows.Forms.MenuItem[] { this.menuItem1, this.menuItem2, this.menuItem3, this.menuItem4 });

            // Initialize menuItem1
            this.menuItem1.Index = 3;
            this.menuItem1.Text = "E&xit";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);

            // Initialize menuItem2
            this.menuItem2.Index = 2;
            this.menuItem2.Text = "Open";
            this.menuItem2.Click += new System.EventHandler(this.open_dialog);

            // Initialize menuItem3
            this.menuItem3.Index = 1;
            this.menuItem3.Text = "DisConnect";
            this.menuItem3.Click += new System.EventHandler(this.notify_disconnectbtn );

            // Initialize menuItem4
            this.menuItem4.Index = 0;
            this.menuItem4.Text = "Connect";
            this.menuItem4.Click += new System.EventHandler(this.notify_connectbtn );


            // Set up how the form should be displayed.
            // this.ClientSize = new System.Drawing.Size(292, 266);
            //this.Text = "Notify Icon Example";

            // Create the NotifyIcon.
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);

            // The Icon property sets the icon that will appear
            // in the systray for this application.
            notifyIcon1.Icon = this.Icon;

            // The ContextMenu property sets the menu that will
            // appear when the systray icon is right clicked.
            notifyIcon1.ContextMenu = this.contextMenu1;

            // The Text property sets the text that will be displayed,
            // in a tooltip, when the mouse hovers over the systray icon.
            notifyIcon1.Text = "Kashan University HS manager";
            notifyIcon1.Visible = true;

            // Handle the DoubleClick event to activate the form.
            notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);

            timer1.Enabled = true;
            btn_connection_check_Click(sender, e);

            read_data();


            Update_checker();
        }

        private void Update_checker()
        {
            try
            {
                using (System.Net.WebClient wc = new WebClient())
                {
                    wc.Proxy = new WebProxy();
                    string newest_version = wc.DownloadString("https://i3130002.gitlab.io/Kashan-University_HS_Manager/Last_version.txt");
                   
                     
                    int last_version;
                    if (int.TryParse(newest_version, out last_version))
                    {
                        if (last_version> version )
                        {
                            lklb_update.Links.Clear();
                            lklb_update.Text = "Newer version avalible";
                            lklb_update.Links.Add(0, lklb_update.Text.Length, "https://i3130002.gitlab.io/Kashan-University_HS_Manager/");
                            lklb_update.Font = new Font("Arial", 20, FontStyle.Bold);
                            lklb_update.Left = this.Width - lklb_update.Width - 10;
                            lklb_update.Top = this.Height - lklb_update.Height - 40;
                        }
                        else
                        {
                            lklb_update.Links.Clear();
                            lklb_update.Text = "Home page";
                            lklb_update.Links.Add(0, lklb_update.Text.Length, "https://i3130002.gitlab.io/Kashan-University_HS_Manager/");
                            lklb_update.Left = this.Width - lklb_update.Width - 30;
                            lklb_update.Top = this.Height - lklb_update.Height - 45;
                        }
                       
                    }
                }
            }
            catch (Exception ex)
            {

                lklb_update.Text = "Connection or update server problem "+ex.Message;
                lklb_update.Left = this.Width - lklb_update.Width - 10;
                lklb_update.Top = this.Height - lklb_update.Height - 40;
            }


        }
        private void read_data()
        {

            try
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader("data"))
                {

                    string line = "";
                    do
                    {
                        line = sr.ReadLine();
                        if (line != null)
                        {
                            if (line.Split(':').Length == 2)
                            {
                                dataGridView1.Rows.Add(line.Split(':')[0], line.Split(':')[1]);

                            }
                        }
                    } while (line != null);
                    sr.Close();
                }

            }
            catch (Exception ex)
            {
                Application.DoEvents();
                MessageBox.Show("data file not exist" + Environment.NewLine + "So we consider you as a new user." + Environment.NewLine + "wellcome. :D" + Environment.NewLine
                    + "You can add your HS list to the gridview (it has auto SAVE :D )" + Environment.NewLine + "By double clicking in it it will connect :D" + Environment.NewLine
                    + "Is there any thing else you may want ? email or countribute with the project :D");
            }
        }
        private void open_dialog(object sender, EventArgs e)
        {
            notifyIcon1_DoubleClick(sender, e);
        }
        private void notify_connectbtn(object sender, EventArgs e)
        {
            connect_to_first_allowed();
        }

        private Boolean connect_to_first_allowed(int current=0,string  log="")
        {
            int y = current;
            string user = dataGridView1.Rows[y].Cells[0].Value.ToString();
            string pass = dataGridView1.Rows[y].Cells[1].Value.ToString();
            if (hs.is_uni_internet() == "false")
            {
                reload_status();
                throw new Exception("you are not connected to Kashan uni HS.");
                

                
            }
            else if (hs.is_uni_internet() == "true" && hs.is_login())
            {
                MessageBox.Show("you were connected. try to disconnect first");
                return true;
            }
            else
            {
                string res = hs.login(user, pass);
                if (res != "logedin")
                {
                    log+=string.Format("({1},{2}):error:" ,user,pass )+ Environment.NewLine + res+ Environment.NewLine;
                    if (current <= dataGridView1.Rows.Count - 1)
                      return   connect_to_first_allowed(current++, log);

                    MessageBox.Show("no working username and password were found. log:" + Environment.NewLine + log);
                    return false;

                }
                else
                {

                    reload_status();
                    gridview_color_fill();
                    MessageBox.Show("Connected. User:" + hs.logedin_user_name+Environment.NewLine +"log:"+log);

                }
                return false;

            }
        }
        private void notify_disconnectbtn(object sender, EventArgs e)
        {
            hs.logout();
            btn_connection_check_Click(sender, e);
            if (hs.is_loged_in)
                MessageBox.Show("failed to logeout");
            else
                MessageBox.Show("logedout");

            gridview_color_fill();
        }
        private void menuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
        }



        Hs hs = new Hs();


        private void btn_connection_check_Click(object sender, EventArgs e)
        {
            reload_status();
        }


        private void reload_status()
        {
            string status = hs.is_uni_internet();
            if (status == "true")
            {
                lb_is_uni_hs.Text = "KashanU Network";
                if (hs.is_login())
                {
                    lb_status.Text = "Connected to HS";
                    btn_disconnect.Enabled = true;
                    btn_connect.Enabled = false;
                    try
                    {
                        lb_status.Text += Environment.NewLine + "User:" +
                            hs.get_logedin_username();
                        gridview_color_fill();
                    }
                    catch (Exception)
                    {

                        lb_status.Text += Environment.NewLine + "User:unknown. please check the code at HS.get_logedin_username()";
                    }
                }
                else
                {
                    lb_status.Text = "Disconnected from HS";
                    btn_disconnect.Enabled = false;
                    btn_connect.Enabled = true;


                }

            }
            else if (status == "false")
            {
                lb_is_uni_hs.Text = "Connected to some where else";
                btn_disconnect.Enabled = false;
                btn_connect.Enabled = false;

            }
            else
            {
                lb_is_uni_hs.Text = "Network not avalible or proxy settings are bad" + Environment.NewLine + status;
                btn_disconnect.Enabled = false;
                btn_connect.Enabled = false;

            }

        }
        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            grid_save();
        }

        private void grid_save()
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter("data");
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                if (dataGridView1.Rows[i].Cells[0] != null)
                    sw.Write(dataGridView1.Rows[i].Cells[0].Value);
                else
                    sw.Write("");
                sw.Write(":");

                if (dataGridView1.Rows[i].Cells[0] != null)
                    sw.Write(dataGridView1.Rows[i].Cells[1].Value);
                else
                    sw.Write("");

                sw.Write(Environment.NewLine);

            }
            sw.Close();
        }

        private void dataGridView1_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            grid_save();
        }

        private void lklb_update_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(lklb_update.Links[0].LinkData.ToString());
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;

            }
            else
            {
                this.ShowInTaskbar = true;

            }

        }

        

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < dataGridView1.Rows.Count)
            {
                btn_connect.Enabled = true;
            }
            else
            {
                btn_connect.Enabled = false;
            }
        }

        private void btn_connect_Click(object sender, EventArgs e)
        {


            int y = dataGridView1.SelectedCells[0].RowIndex;
            string user = dataGridView1.Rows[y].Cells[0].Value.ToString();
            string pass = dataGridView1.Rows[y].Cells[1].Value.ToString();
            if (hs.is_uni_internet() == "false")
            {
                MessageBox.Show("you are not connected to Kashan uni HS.");

                reload_status();

            }
            else if (hs.is_uni_internet() == "true" && hs.is_login())
            {
                MessageBox.Show("you were connected. try to disconnect first");
            }
            else
            {
                string res = hs.login(user, pass);
                if (res != "logedin")
                {
                    MessageBox.Show("error" + Environment.NewLine + res);
                }
                else
                {

                    reload_status();
                }
            }
        }

        private void gridview_color_fill()
        {
            if (hs.is_loged_in)
            {
                foreach(DataGridViewRow row in dataGridView1.Rows)
                {
                    if (row.Cells["username"].Value != null)
                        if( row.Cells["username"].Value.ToString()== hs.logedin_user_name)
                        {
                            row.DefaultCellStyle .BackColor= btn_connect.BackColor;
                        
                        }
                        else
                        {
                            row.DefaultCellStyle.BackColor = Color.White;

                        }

                }
            }
        }

        private void process_ibsng_data_btn(object sender, EventArgs e)
        {
            MessageBox.Show("wait for end");
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                Dictionary<string, string> res = hs.get_ibsng_data(dataGridView1.Rows[i].Cells[0].Value.ToString(), dataGridView1.Rows[i].Cells[1].Value.ToString());
                dataGridView1.Rows[i].Cells["size"].Value = res["total"];
                dataGridView1.Rows[i].Cells["Recharge_date"].Value = res["date"];
                dataGridView1.Rows[i].Cells["Used"].Value = res["used"];

                Application.DoEvents();
            }
            MessageBox.Show("End");
        }

        private void btn_disconnect_Click(object sender, EventArgs e)
        {
            hs.logout();
            btn_connection_check_Click(sender, e);
            gridview_color_fill();
        }
    }
}
