﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Hs
    {
        const string login_url = "http://hs.kashanu.ac.ir/login?";

        const string status_url = "http://hs.kashanu.ac.ir/status";
        const string status_login_key = "<input type=\"submit\" value=\"log off\">";


        const string logout_url = "http://hs.kashanu.ac.ir/logout?";
        const string logout_key = "value=\"ورود\"";

        const string ibsng_url = "http://172.16.255.250/IBSng/user/";
        const string ibsng_login_key = "IBSng | ورود به سیستم";

        const string ibsng_home = "http://172.16.255.250/IBSng/user/home.php";
        const string ibsng_home_key = "IBSng | ورود به سیستم";


       public Boolean is_loged_in = false;
       public string logedin_user_name = "";






        public string is_uni_internet()
        {

            NameValueCollection nv = new NameValueCollection();
            try
            {
                string response = post(ibsng_url, nv);
                if (response.IndexOf(ibsng_login_key) > 0)
                    return "true";
                return "false";
            }
            catch (Exception ex)
            {

                return ex.Message;
            }

        }

        public string login(string username, string password)
        {
            NameValueCollection nv = new NameValueCollection();
            nv.Add("username", username);
            nv.Add("password", password);
            string doc = post(login_url, nv);
            if (!is_login())
                return get_error(doc);
            return "logedin";
        }
        private string get_error(string doc)
        {
            int error_start = doc.IndexOf("<span style=\"color: #FF8080; font-size: 12px;\"><BR />");
            if (error_start > 0)
            {
                error_start += "<span style=\"color: #FF8080; font-size: 12px;\"><BR />".Length;
                doc = doc.Substring(error_start);
                int error_end = doc.IndexOf("</span>");
                doc = doc.Substring(0, error_end);
                return doc;
            }
            return "no error found";
        }
        public string logout()
        {
            NameValueCollection nv = new NameValueCollection();

            return post(logout_url, nv);

        }

        public Boolean is_logout()
        {
            NameValueCollection nv = new NameValueCollection();

            string response = post(status_url, nv);
            if (response.IndexOf(logout_key) > 0)
                return true;
            return false;


        }
        public Boolean is_login()
        {
            NameValueCollection nv = new NameValueCollection();

            string response = post(status_url, nv);
            if (response.IndexOf(status_login_key) > 0)
            {
                is_loged_in = true;
                return true;
            }
            else
            {
                is_loged_in = false;
                return false;
            }


        }
        public string get_logedin_username()
        {
            NameValueCollection nv = new NameValueCollection();

            string response = post(status_url, nv);
            string username = get_string_between_tags(response, ";\">Welcome ", "!</div><br>");

            if (username.Length > 1)
                logedin_user_name = username;
            return username;

            throw
                new MissingFieldException("we cant find any thing between " + "<br><div style=\"text - align: center; \">Welcome " + " and " + "!</div><br>");


        }

        public Dictionary<string, string> get_ibsng_data(string username, string password)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();
            byte[] response = null;
            using (WebClient client = new WebClient())
            {
                NameValueCollection nv = new NameValueCollection();
                nv.Add("normal_username", username);
                nv.Add("normal_password", password);
                client.Headers.Add("Host", "172.16.255.250");


                client.Proxy = new WebProxy();
                client.Headers.Add(HttpRequestHeader.Cookie, "IBS_SESSID=dqqfk2jo5imm889dbpira412b6; session_id=7e70e6ac7b97a7cb7d98ebc01dcb604179a32fdb");
                response = client.UploadValues(ibsng_url, "POST", nv);
            }
            string response_str = System.Text.Encoding.UTF8.GetString(response);
            if (response_str.IndexOf("IBSng | ورود به سیستم") > 0)
            {
                ret.Add("total", "no ibsng");
                ret.Add("used", "no ibsng");
                ret.Add("date", "no ibsng");
            }
            else
            {
                if (response_str.IndexOf("اطلاعات کاربر") > 0) //successfully logedin.
                {
                    if (response_str.IndexOf("راکد - فارغ التحصیل") > 0)
                    {
                        ret.Add("total", "راکد - فارغ التحصیل");
                        ret.Add("used", "راکد - فارغ التحصیل");
                        ret.Add("date", "راکد - فارغ التحصیل");

                    }
                    else
                    {



                        string remained = get_chiled_number(response_str, "<td class=\"Form_Content_Row_Right_dark\" >", "</sub></td>", 1);
                        if (remained == null)
                            remained = "miss tags";
                        remained = remained.Replace("<sub>", " ");

                        string used = get_chiled_number(response_str, "<td class=\"Form_Content_Row_Right_light\" >", "</sub></td>", 2);
                        if (used == null)
                            used = "miss tags";
                        used = used.Replace("<sub>", " ");

                        string recharge_date = get_chiled_number(response_str, "<td class=\"Form_Content_Row_Right_dark\" >", "</td>", 2);
                        if (recharge_date == null)
                            recharge_date = "miss tags";

                        ret.Add("total", remained);
                        ret.Add("used", used);
                        ret.Add("date", recharge_date);


                    }
                }
                else
                {
                    ret.Add("total", "error");
                    ret.Add("used", "error");
                    ret.Add("date", "error");
                }

            }
            if (ret.Count == 0)
            {
                ret.Add("total", "no ibsng");
                ret.Add("used", "no ibsng");
                ret.Add("date", "no ibsng");
            }
            return ret;
        }

        private int IndexOfNth(string str, string value, int nth = 1)
        {
            if (nth <= 0)
                throw new ArgumentException("Can not find the zeroth index of substring in string. Must start with 1");
            int offset = str.IndexOf(value);
            for (int i = 1; i < nth; i++)
            {
                if (offset == -1) return -1;
                offset = str.IndexOf(value, offset + 1);
            }
            return offset;
        }
        private string get_string_between_tags(string doc, string tag1, string tag2)
        {
            int error_start = doc.IndexOf(tag1);
            if (error_start > 0)
            {
                error_start += tag1.Length;
                doc = doc.Substring(error_start);
                int error_end = doc.IndexOf(tag2);
                if (error_end < 0)
                    return null;

                doc = doc.Substring(0, error_end);
                return doc;
            }
            return null;
        }
        private string get_chiled_number(string doc, string before, string after, int children_version)
        {
            int error_start = IndexOfNth(doc, before, children_version);
            if (error_start > 0)
            {
                error_start += before.Length;
                doc = doc.Substring(error_start);
                int error_end = doc.IndexOf(after);
                if (error_end < 0)
                    return null;

                doc = doc.Substring(0, error_end);
                return doc;
            }
            return null;
        }

        private string post(string uri, NameValueCollection collection)
        {
            byte[] response = null;
            using (WebClient client = new WebClient())
            {
                client.Proxy = new WebProxy();
                response = client.UploadValues(uri, collection);
            }
            return System.Text.Encoding.UTF8.GetString(response);
        }
    }
}
